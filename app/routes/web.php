<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FormMemberAPIController@show');

/**
 * Admin Login Route
 */
Route::get('/login', function() {
    return view('auth.login');
})->name('login-view');

/**
 * Admin Dashboard Route
 */
Route::group(['prefix' => 'admin', 'middleware' => 'admin.jwt'], function() {
    // Dashboard
    Route::get('dashboard', 'DashboardViewController@main_dashboard_view')->name('dashboard');
    // Data Member
    Route::get('member/data', 'DashboardViewController@data_member_view')->name('data-member');
    // Data Admin
    Route::get('users/data', 'DashboardViewController@data_admin_view')->name('data-admin');
    Route::get('users/create', 'DashboardViewController@create_admin_view')->name('create-admin');
});

/**
 * API Route
 */
Route::group(['prefix' => 'api/v2'], function() {
    // Login
    Route::post('login', 'AdminLoginAPIController@login')->name('login');
    // Form Member
    Route::post('form-member', 'FormMemberAPIController@create')->name('form-member');

    Route::group(['middleware' => 'admin.jwt'], function() {
        // Logout
        Route::get('logout', 'AdminLoginAPIController@logout')->name('logout');
        // Get Member Data
        Route::get('member/get', 'DashboardViewController@get_member_data');
        // Get Admin
        Route::get('admin/get', 'DashboardViewController@get_admin_data');
        Route::post('admin/store', 'DashboardViewController@store_admin_data')->name('store-admin');
    });
});