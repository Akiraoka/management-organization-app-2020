$(document).ready(() => {
    // Create Variable
    const adminMenu  = $('#adminMenu');
    const adminLink   = $('#adminLink');
    const adminOpen  = $('#adminOpen');
    const table      = $('#table');

    // Sidebar Link
    adminOpen.addClass('menu-open');
    adminMenu.addClass('active');
    adminLink.addClass('active');

    // Create Function
    let get_data_admin = () => {
        t = $(table).DataTable({
            responsive: true,
            columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: 4 }
            ],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/api/v2/admin/get',
                method: 'GET',
            },
            columnDefs: [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            order: [[ 1, 'asc']],
            columns: [
                { data: "id" },
                { data: "username" },
                { data: "level_name" },
                { data: "action" },
            ]
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    }

    // Call Function
    get_data_admin();
});