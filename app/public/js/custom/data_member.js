$(document).ready(() => {
    // Create Variable
    const memberMenu = $('#memberMenu');
    const memberLink = $('#memberLink');
    const memberOpen = $('#memberOpen');
    const table      = $('#table');

    // Sidebar Link
    memberOpen.addClass('menu-open');
    memberMenu.addClass('active');
    memberLink.addClass('active');

    // Create Function
    let get_data_member = () => {
        t = $(table).DataTable({
            responsive: true,
            columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: 4 }
            ],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/api/v2/member/get',
                method: 'GET',
            },
            columnDefs: [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            order: [[ 1, 'asc']],
            columns: [
                { data: "id" },
                { data: "provinsi" },
                { data: "kabupaten" },
                { data: "kecamatan" },
                { data: "kelurahan" },
                { data: "total" },
            ]
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    }

    // Call Function
    get_data_member();
});