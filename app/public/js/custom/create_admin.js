$(document).ready(() => {
    // Create Variable
    const adminMenu  = $('#adminMenu');
    const adminLink   = $('#adminStoreLink');
    const adminOpen  = $('#adminOpen');

    // Sidebar Link
    adminOpen.addClass('menu-open');
    adminMenu.addClass('active');
    adminLink.addClass('active');

    // Set first value selected location
    if($('#level_id').find(":selected").val() == 'provinsi') {
        // Set Attr
        $('#defaultLocation').attr('hidden', true);
        $('#kelurahan').attr('hidden', true);
        $('#kecamatan').attr('hidden', true);

        // If Provinsi Level Selected
        $('#provinsi').attr('hidden', false);
    }

    // Event Function
    $('#level_id').on('change', () => {
        if($('#level_id').find(":selected").val() == 'provinsi') {
            // Set Attr
            $('#defaultLocation').attr('hidden', true);
            $('#kelurahan').attr('hidden', true);
            $('#kecamatan').attr('hidden', true);

            // If Provinsi Level Selected
            $('#provinsi').attr('hidden', false);
        }else if($('#level_id').find(":selected").val() == 'kecamatan') {
            // Set Attr
            $('#defaultLocation').attr('hidden', true);
            $('#kelurahan').attr('hidden', true);
            $('#provinsi').attr('hidden', true);

            // If Provinsi Level Selected
            $('#kecamatan').attr('hidden', false);
        }else if($('#level_id').find(":selected").val() == 'kelurahan') {
            // Set Attr
            $('#defaultLocation').attr('hidden', true);
            $('#kecamatan').attr('hidden', true);
            $('#provinsi').attr('hidden', true);

            // If Provinsi Level Selected
            $('#kelurahan').attr('hidden', false);
        }
    });
});