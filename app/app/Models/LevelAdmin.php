<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelAdmin extends Model
{
    protected $table      = 'level_admins';
    protected $primaryKey = 'id';

    /**
     * Create Relationship
     */

    public function admin()
    {
        //
    }
}
