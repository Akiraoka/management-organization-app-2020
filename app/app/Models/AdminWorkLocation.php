<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminWorkLocation extends Model
{
    protected $table      = 'admin_work_location';
    protected $primaryKey = 'id';
}
