<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table      = 'members';
    protected $primaryKey = 'id';

    /**
     * Create Relationship
     */

    public function provinsi()
    {
        $this->belongsTo('App\Models\Provinsi', 'provinsi_id', 'id');
    }

    public function kecamatan()
    {
        $this->belongsTo('App\Models\Kecamatan', 'kecamatan_id', 'id');
    }

    public function kelurahan()
    {
        $this->belongsTo('App\Models\Kelurahan', 'kelurahan_id', 'id');
    }

    public function Kabupaten()
    {
        $this->belongsTo('App\Models\Kabupaten', 'kabupaten_id', 'id');
    }
}
