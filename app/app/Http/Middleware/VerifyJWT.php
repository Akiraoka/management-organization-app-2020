<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Exception;

class VerifyJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if(!Session::get('user_id')) throw new Exception('session lost');
        }catch (Exception $e) {
            return redirect()->route('login-view');
        }

        return $next($request);
    }
}
