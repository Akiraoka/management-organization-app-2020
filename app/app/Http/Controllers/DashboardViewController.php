<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Admin;
use App\Models\Member;
use App\Models\LevelAdmin;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Domicile;
use App\Models\AdminWorkLocation;

use Session;
use DataTables;

use Carbon\Carbon;

class DashboardViewController extends Controller
{
    /**
     * API VIEW
     * Dashboard View Controller
     */

    /**
     * Main Dashboard
     */
    public function main_dashboard_view()
    {
        try {
            // Get User 
            $get_user = Admin::where('id', Session::get('user_id'))->first();
            // Count New Member
            $count_new_member = Member::where('created_at', 'like' ,'%'.date('Y-m-d').'%')->count();
            // Count Member
            $count_member = Member::count();

        } catch (Exception $e) {
            return redirect()->route('login-view');
        }

        return view('dashboard.app', [
            'user'             => $get_user,
            'count_new_member' => $count_new_member,
            'count_member'     => $count_member
        ]);
    }

    public function data_member_view()
    {
        try {
            // Get User 
            $get_user = Admin::where('id', Session::get('user_id'))->first();
                
        } catch (Exception $e) {
            return redirect()->route('login-view');
        }
        return view('dashboard.data_member', [
            'user' => $get_user
        ]);
    }

    public function data_admin_view()
    {
        try {
            // Get User 
            $get_user = Admin::where('id', Session::get('user_id'))->first();
                
        } catch (Exception $e) {
            return redirect()->route('login-view');
        }
        return view('dashboard.data_admin', [
            'user' => $get_user
        ]);
    }

    public function create_admin_view()
    {
        try {
            // Get User 
            $get_user = Admin::where('id', Session::get('user_id'))->first();
            // Get Level Admin
            $get_level_admin = LevelAdmin::where('name', '!=', 'pusat')
                ->where('name', '!=', 'kabupaten')
                ->get();
            // Get Provinsi 
            $get_provinsi = Provinsi::all();
            // Get Kecamatan
            $get_kecamatan = Kecamatan::all();
            // Get Kelurahan
            $get_kelurahan = Kelurahan::all();

        } catch (Exception $e) {
            return redirect()->route('login-view');
        }
        return view('dashboard.create_admin', [
            'user'      => $get_user,
            'provinsi'  => $get_provinsi,
            'kecamatan' => $get_kecamatan,
            'kelurahan' => $get_kelurahan,
            'level'     => $get_level_admin
        ]);
    }
    /**
     * API LOGIC
     */

    public function get_member_data(Request $request)
    {
        try {
            // Get Data Member
            $get_member = \DB::table('members')
                ->join('domiciles', 'domiciles.id', 'members.domiciles_id')
                ->orderBy('members.id', 'DESC')
                ->get();
                
        } catch (Exception $e) {
            return redirect()->route('login-view');
        }

        // Set DataTable
        return DataTables::of($get_member)
            ->addColumn('provinsi', function($row) {
                $provinsi = Provinsi::where('id', $row->provinsi_id)->first();
                
                return $provinsi->name;
            })
            ->addColumn('kabupaten', function($row) {
                $kabupaten = Kabupaten::where('id', $row->kabupaten_id)->first();
                
                return $kabupaten->name;
            })
            ->addColumn('kecamatan', function($row) {
                $kecamatan = Kecamatan::where('id', $row->kecamatan_id)->first();
                
                return $kecamatan->name;
            })
            ->addColumn('kelurahan', function($row) {
                $provinsi = Kelurahan::where('id', $row->kelurahan_id)->first();
                
                return $provinsi->name;
            })
            ->addColumn('total', function($row) {                
                return 0;
            })
            ->rawColumns(['provinsi','kabupaten','kecamatan','kelurahan','total'])
            ->make(true);
    }

    public function get_admin_data(Request $request)
    {
        try {
            // Get Data Member
            $get_admin = \DB::table('admins')
                ->join('level_admins', 'level_admins.id', 'admins.level_admin_id')
                ->select('admins.*', 'level_admins.name as level_name')
                ->get();
                
        } catch (Exception $e) {
            return redirect()->route('login-view');
        }

        // Set DataTable
        return DataTables::of($get_admin)
            ->addColumn('action', function($row) {                
                $btn = '<button class="btn btn-primary" data-id="'. $row->id .'"><i class="fas fa-edit"></i></button>';
                return $btn . '<button class="btn btn-danger ml-2" data-id="'. $row->id .'"><i class="fas fa-trash"></i></button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store_admin_data(Request $request) 
    {
        // Create Variable
        $username = htmlspecialchars($request->username);
        $password = htmlspecialchars($request->password);
        $level    = htmlspecialchars($request->level);
        
        // Try Catch Block
        try {
            // Check Level
            if($level == 'provinsi') {

                $location  = htmlspecialchars($request->level_provinsi);
                // Get Provinsi
                $provinsi = Provinsi::where('name', $location)->first();

            }else if($level == 'kecamatan') {

                $location = htmlspecialchars($request->level_kecamatan);
                // Get Kecamatan
                $kecamatan = Kecamatan::where('name', $location)->first();

            }else if($level == 'kelurahan') {

                $location = htmlspecialchars($request->level_kelurahan);
                // Get Kelurahan
                $kelurahan = Kelurahan::where('name', $location)->first();

            }

            // Find Level Id
            $find_level_id = LevelAdmin::where('name', $level)->first();

            // Check Work Location 
            $work_location = AdminWorkLocation::where('is_provinsi', $location)
                ->orWhere('is_kabupaten', $location)
                ->orWhere('is_kelurahan', $location)
                ->first();
            if(!$work_location) {
                // Store Location
                $store = \DB::table('admin_work_location')->insert([
                    'is_provinsi'  => isset($provinsi) ? $provinsi->id : null,
                    'is_kecamatan' => isset($kecamatan) ? $kecamatan->id : null,
                    'is_kelurahan' => isset($kelurahan) ? $kelurahan->id : null,
                    'created_at'   => Carbon::now('Asia/Jakarta')
                ]);
                // Check Error
                if (!$store) throw new Exception('Storing Data Failed');
            }
            // Store Data
            $store = \DB::table('admins')->insert([
                'username'         => $username,
                'password'         => hash('sha256', $password),
                'level_admin_id'   => $find_level_id->id,
                'location_work_id' => ($work_location) ? $work_location->id : \DB::getPdo()->lastInsertId(),
                'created_at'       => Carbon::now('Asia/Jakarta')
            ]);
        } catch (Exception $e) {
            // Error Response
            $msg = [
                'type'    => 'alert-danger',
                'hasFail' => true,
                'msg'     => $e->getMessage()
            ];

            return redirect()->back()->with($msg);
        }

        // Success Response
        $msg = [
            'type'    => 'alert-success',
            'hasFail' => true,
            'msg'     => 'Data has Stored'
        ];

        return redirect()->back()->with($msg);
    }
}
