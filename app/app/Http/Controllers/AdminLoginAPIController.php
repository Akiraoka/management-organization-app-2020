<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Admin;
use Session;

use Hash;

class AdminLoginAPIController extends Controller
{
    /**
     * Login API
     */

    public function login(Request $request)
    {
        // Create Variable
        $username = $request->username;
        $password = $request->password;

        // Get Admin Data
        $admin = Admin::where('username', $username)->first();
        // Check Admin Login
        if($admin) {
            // Check Password
            $hash_password = hash('sha256', $password);

            if($admin->password == $hash_password) {
                // Push Session
                Session::put('user_id', $admin->id);
                // Login Success
                return redirect()->route('dashboard');
            }else {
                // Login Failed
                $respMsg = [
                    'hasFail'     => true,
                    'responseMsg' => 'Username / Password Salah'
                ];

                return redirect()->back()->with($respMsg);
            }
        }else {
            return redirect()->back()->with(['hasFail' => true, 'responseMsg' => 'Username / Password Salah']);
        }
    }  
    
    public function logout()
    {
        Session::flush();

        return redirect()->route('login-view');
    }
}
