<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Member;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Domicile;

use Exception;

use Carbon\Carbon;

class FormMemberAPIController extends Controller
{
    /**
     * Form Member API
     */

    public function show(Request $request)
    {
        // Get Provinsi
        $provinsi = Provinsi::all();
        // Get Kabupaten
        $kabupaten = Kabupaten::all();
        // Get Kecamatan
        $kecamatan = Kecamatan::all();
        // Get Kelurahan
        $kelurahan = Kelurahan::all();

        return view('index', [
            'provinsi'  => $provinsi,
            'kabupaten' => $kabupaten,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan
        ]);
    }

    public function create(Request $request)
    {
        // Create Variable
        $id_number = htmlspecialchars($request->ktp);
        $fullname  = htmlspecialchars($request->fullname);
        $phone     = htmlspecialchars($request->phone);
        $provinsi  = htmlspecialchars($request->provinsi);
        $kabupaten = htmlspecialchars($request->kabupaten);
        $kecamatan = htmlspecialchars($request->kecamatan);
        $kelurahan = htmlspecialchars($request->kelurahan);

        try {
            // Validate 
            if(strlen($id_number) > 16 || strlen($id_number) < 16) throw new Exception('Nomor KTP Harus 16 Karakter');

            // Store Domiciles
            $store = \DB::table('domiciles')->insert([
                'provinsi_id'  => $provinsi,
                'kabupaten_id' => $kabupaten,
                'kecamatan_id' => $kecamatan,
                'kelurahan_id' => $kelurahan,
                'created_at'   => Carbon::now('Asia/Jakarta')
            ]);
            // Store Member
            $store = \DB::table('members')->insert([
                'id_card'      => $id_number,
                'name'         => $fullname,
                'phone'        => $phone,
                'domiciles_id' => \DB::getPdo()->lastInsertId(),
                'created_at'   => Carbon::now('Asia/Jakarta')
                
            ]);
        } catch (Exception $e) {
            // Give Response
            $msg = [
                'hasFail'     => 'true',
                'type'        => 'alert-danger',
                'responseMsg' => $e->getMessage()
            ];
            return redirect()->back()->with($msg);
        }

        // Success Response
        $msg = [
            'hasFail'     => 'false',
            'type'        => 'alert-success',
            'responseMsg' => 'Formulir Berhasil Terkirim!'
        ];
        
        return redirect()->back()->with($msg);

    }
}
