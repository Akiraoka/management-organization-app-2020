@extends('layout.auth.auth_layout')

<!-- Title  -->
@section('title', 'Main Dashboard')

<!-- Content -->
@section('content')
<div class="login-box mx-auto mt-5">
  <div class="login-logo">
    <a href="../../index2.html"><b>Akses Admin</b></a>
  </div>
  <!-- /.login-logo -->
  @if(Session::has('hasFail'))
    <div class="alert alert-danger" role="alert">
      {{ Session::get('responseMsg') }}
    </div>
  @endif
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="{{ route('login') }}" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="username" placeholder="Username" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
@endsection

<!-- External Script -->
@section('script')

@endsection