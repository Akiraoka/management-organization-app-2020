@extends('layout.app')

<!-- Title  -->
@section('title', 'Daftar Anggota Organisasi')

<!-- Content -->
@section('content')
    <div class="container-fluid mt-5">
        <div class="container">
            <div class="col-lg-4 mx-auto">
                <h4 class="text-center pb-2">Daftar Sebagai Anggota</h4>
                
                @if(Session::has('hasFail'))
                <div class="alert {{ Session::get('type') }}" role="alert" id="alertBox">
                    {{ Session::get('responseMsg') }}
                </div>
                @endif
                <div class="card">
                    <!-- Form Member  -->
                    <div class="card-body">
                        <form action="{{ route('form-member') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>NO. KTP</label>
                                <input type="number" class="form-control" name="ktp" required>
                            </div>
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="fullname" required>
                            </div>
                            <div class="form-group">
                                <label>No. Handphone</label>
                                <input type="number" class="form-control" name="phone" required>
                            </div>
                            <label>Domisili</label>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select name="provinsi" class="form-control" required>
                                            @foreach($provinsi as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <select name="kabupaten" class="form-control" required>
                                            @foreach($kabupaten as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select name="kecamatan" class="form-control" required>
                                            @foreach($kecamatan as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <select name="kelurahan" class="form-control" required>
                                            @foreach($kelurahan as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success btn-block">Kirim Permintaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- External Script -->
@section('script')
    <script>
        $(document).ready(function() {
            // Create Variable
            const idNumber = $('#idNumber');
        });
    </script>
@endsection