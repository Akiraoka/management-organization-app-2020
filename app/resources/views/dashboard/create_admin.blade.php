@extends('layout.master')

<!-- Title  -->
@section('title', 'Admin - Daftar Anggota')

<!-- Content -->
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Admin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Anggota</a></li>
              <li class="breadcrumb-item active">Tambah Anggota</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('store-admin') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Level</label>
                            <select name="level" class="form-control" id="level_id">
                                @foreach($level as $row)
                                <option value="{{ $row->name }}">{{ ucfirst($row->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <label for="">Wilayah Kerja</label>
                            <select name="" id="defaultLocation" class="form-control" disabled>
                                <option value="">Wilayah</option>
                            </select>
                            <!-- Provinsi -->
                            <select name="level_provinsi" class="form-control" id="provinsi" hidden>
                                @foreach($provinsi as $row)
                                <option value="{{ $row->name }}">{{ ucfirst($row->name) }}</option>
                                @endforeach
                            </select>
                            <!-- Kecamatan -->
                            <select name="level_kecamatan" class="form-control" id="kecamatan" hidden>
                                @foreach($kecamatan as $row)
                                <option value="{{ $row->name }}">{{ ucfirst($row->name) }}</option>
                                @endforeach
                            </select>
                            <!-- Kelurahan -->
                            <select name="level_kelurahan" class="form-control" id="kelurahan" hidden>
                                @foreach($kelurahan as $row)
                                <option value="{{ $row->name }}">{{ ucfirst($row->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group pt-3">
                            <button class="btn btn-outline-success">Create</button>
                        </div>
                    </form>
                </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
    </section>
</div>
@endsection

<!-- External Script -->
@section('script')
    <script src="{{ asset('js/custom/create_admin.js') }}"></script>
@endsection