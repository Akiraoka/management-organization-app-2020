@extends('layout.master')

<!-- Title  -->
@section('title', 'Admin - Daftar Admin')

<!-- Content -->
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Daftar Admin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Anggota</a></li>
              <li class="breadcrumb-item active">Daftar Anggota</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-hover" id="table">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Username</th>
                                <th>Level</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
    </section>
</div>
@endsection

<!-- External Script -->
@section('script')
    <script src="{{ asset('js/custom/data_admin.js') }}"></script>
@endsection