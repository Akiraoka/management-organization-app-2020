<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <!-- Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Admin LTE  -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    <!-- DataTable -->
    <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
    <!-- Fontawesome  -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
</head>
<body>
    <!-- Main Header -->
    @include('layout.header')
    <!-- Sidebar -->
    @include('layout.sidebar')
    <!-- Main Content -->
    @yield('content')
    <!-- Footer -->
    @include('layout.footer')

    <!-- Script -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>

    @yield('script')
</body>
</html>