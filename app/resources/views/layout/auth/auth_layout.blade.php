<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <!-- Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Admin LTE  -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
</head>
<body>

    <!-- Main Content -->
    @yield('content')

    <!-- Script -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/adminlte.js') }}"></script>
</body>
</html>