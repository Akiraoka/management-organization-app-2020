<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminWorkLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_work_location', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('is_provinsi')->nullable();
            $table->unsignedBigInteger('is_kabupaten')->nullable();
            $table->unsignedBigInteger('is_kecamatan')->nullable();
            $table->unsignedBigInteger('is_kelurahan')->nullable();
            $table->timestamps();

            $table->foreign('is_provinsi')->references('id')->on('provinsi');
            $table->foreign('is_kabupaten')->references('id')->on('kabupaten');
            $table->foreign('is_kecamatan')->references('id')->on('kecamatan');
            $table->foreign('is_kelurahan')->references('id')->on('kelurahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_work_location');
    }
}
